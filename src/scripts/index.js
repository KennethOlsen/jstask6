import '../styles/index.scss';

$(document).ready(()=>{
    
    console.log('Hello jQuery');
});
    /**
     * Documentation for the slider
     * https://bxslider.com/examples/auto-show-start-stop-controls/ 
     */ 

     /**
     * Documentation for the Lightbox - Fancybox
     * See the section 5. Fire plugin using jQuery selector.
     * http://fancybox.net/howto
     */ 

     /**
      * Boostrap Modal (For the Learn More button)
      * https://getbootstrap.com/docs/4.0/components/modal/
      */
    



/**
 * REMEMBER!!
 * Declaring Global functions that are accessible in your HTML.
 */ 

 var query = "Fish";
 var pageNumber = 1;
 var per_page = 15;

 

const apiKey = "563492ad6f917000010000013b992435084844b08ae5a42ca698cafa";
var baseURL = `https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query=${query}&${per_page}&${pageNumber}`;


$(document).ready(function() {
$.ajax({
    type: 'GET',
    headers: {
    "Authorization": `Bearer ${apiKey}`
    },
    url: baseURL
    }).done(data => {
    renderPhotos(data.photos);
});

});


// Render all the photos from the API
function renderPhotos(photos) {
    $.each(photos, (i, photo)=>{
        $(".container").append(createCard(photo));
    });
   }

   function createCard(photo) {
    return `
        <div class="col-4">
            <div class="card h-100" style="18rem;">
                <div class="card-img">
                    <img class="card-img-top" src="${photo.src.medium}"/>
                </div>
        
                <div class="card-body">
                    <h5>${photo.photographer}</h5>
                </div>
            </div>
        </div>
    `;
   };  


        $("#searchButton").click(function(){
        var value = $("#myInput").val();
          query = value;
          baseURL = `https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query=${query}&${per_page}&${pageNumber}`;
          $.ajax({
            type: 'GET',
            headers: {
            "Authorization": `Bearer ${apiKey}`
            },
            url: baseURL
            }).done(data => {
            $(".container").empty();
            renderPhotos(data.photos);
        });
    });


    // $(".dropdown-toggle").click(function(){
        
    //     $(".dropdown-toggle").val("6");
    //     $(".dropdown-toggle").val("12");
    //     $(".dropdown-toggle").val("24");

    //   } );